<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\StarshipCollection;
use App\Http\Resources\StarshipResource;
use App\Models\Starship;
use Illuminate\Http\Request;

class StarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new StarshipCollection(Starship::with('pilots')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function show(Starship $starship)
    {
        return new StarshipResource($starship->load('pilots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Starship $starship)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function destroy(Starship $starship)
    {
        //
    }

    /**
     * Add pilot startship
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function addPilot(Request $request , Starship $starship){
        try{

            $starship->pilots()->sync($request->pilotId , false);
            return response()->json(['message' => 'Piloto añadido correctamente'], 200);
        }catch(\Exception $e){
            return response()->json(['message' => 'Error'], 500);
        }
    }

     /**
     * Delete pilot startship
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function deletePilot(Request $request , Starship $starship){
        try{
            $starship->pilots()->detach($request->pilotId);
            return response()->json(['message' => 'Piloto eliminado de la relación correctamente'], 200);
        }catch(\Exception $e){
            return response()->json(['message' => 'Error'], 500);
        }
    }
}
