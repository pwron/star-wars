<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Starship;
use App\Models\Pilot;
use Illuminate\Support\Facades\Artisan;

class generateGalaxy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crear-galaxia-muy-muy-lejana';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Que la fuerza te acompañe.';

    /**
     * Instancia del guzzle.
     *
     * @return void
     */

    private $client;

    public function __construct()
    {
        parent::__construct();
        $this->client = new Client(['verify' => false]);
    }

    /**
     * Execute the console command.
     *
     * @return boolean
     */
    public function handle()
    {
        $this->comment('Borrando tablas... o creándolas....');
        Artisan::call('migrate:fresh');

        try {

            $this->comment('Generando naves espaciales.');
            $this->generateStarShips('https://swapi.dev/api/starships');

            $this->comment('-----------------------------------------');
            $this->comment('Naves espaciales creadas.');
            $this->comment('-----------------------------------------');

            $this->comment('Generando pilotos espaciales.');
            $this->generatePilots('https://swapi.dev/api/people');
            $this->comment('-----------------------------------------');
            $this->comment('Pilotos espaciales creados.');
            $this->comment('-----------------------------------------');

            $this->comment('Que la fuerza te acompañe. xD');

            return true;
        } catch (\Exception $e) {
            dd('Espero que no veas esto xD ' . $e->getMessage());
        }
    }

    /**
     * Genera recursivamente las naves por página
     *
     * @return void
     */
    private function generateStarShips($url)
    {
        $response = $this->client->request('GET', $url);
        $responseBody = json_decode($response->getBody());
        $starships = $responseBody->results;

        foreach ($starships as $starship) {
            $this->comment('Nave espacial creada: ' . $starship->name);
            Starship::create([
                'name' => $starship->name,
                'model' => $starship->model,
                'manufacturer' => $starship->manufacturer,
                'cost_in_credits' => $starship->cost_in_credits,
            ]);
        }
        if (!is_null($responseBody->next)) $this->generateStarShips($responseBody->next);
    }

    /**
     * Genera recursivamente los pilotos por página y si tiene naves las sincroniza
     *
     * @return void
     */
    private function generatePilots($url)
    {
        $response = $this->client->request('GET', $url);
        $responseBody = json_decode($response->getBody());
        $pilots = $responseBody->results;

        foreach ($pilots as $pilot) {
            $this->comment('Piloto espacial creado: ' . $pilot->name);
            $p = Pilot::create([
                'name' => $pilot->name,
                'gender' => $pilot->gender,
            ]);
            $p->starships()->sync($this->getIdsStarships($pilot->starships));
        }
        if (!is_null($responseBody->next)) $this->generatePilots($responseBody->next);
    }

    /**
     * Al no haber ids en los modelos de la api de swapi (no valen los de la URL), hay q volver a preguntar por el nombre de la nave para traer nuestro ID local y poder relacionarlo.
     *
     * @return Array
     */
    private function getIdsStarships($starships = [])
    {
        $data = [];
        foreach ($starships as $starship) {
            $response = $this->client->request('GET', $starship);
            $responseBody = json_decode($response->getBody());
            $nombre = $responseBody->name;
            $data[] = Starship::byName($nombre)->select('id')->firstOrFail()->id;
        }
        return $data;
    }
}
