<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Starship;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pilot extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gender'
    ];

    public function starships(){
        return $this->belongsToMany(Starship::class, 'starships_pilots');
    }
}
