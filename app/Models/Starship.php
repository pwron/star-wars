<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pilot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Starship extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'model',
        'manufacturer',
        'cost_in_credits',
    ];

    /**
     * Relaciones
     *
     */
    public function pilots(){
        return $this->belongsToMany(Pilot::class, 'starships_pilots');
    }

     /**
     * Scopes
     *
     */
    public function scopeByName($query , $value){
        return $query->where('name' , $value);
    }
}
