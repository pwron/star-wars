import Vue from 'vue';
import Vuex from 'vuex';

import StarshipsModule from '../Modules/StarshipsModule';
import PilotsModule from '../Modules/PilotsModule';

Vue.use(Vuex);
window.Event = new Vue();

const store = new Vuex.Store({
    state: {
        
    },
    mutations: {

    },
    action:{
        
    },
    modules:{
        StarshipsModule,
        PilotsModule,
    }
});

export default store;