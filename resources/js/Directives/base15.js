export default {
    bind: function (el, binding) {
        let value = binding.value;
        if (!isNaN(value)) {
            let digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "ß", "Þ", "¢", "μ", "¶"];
            let base = 15;
            let output = [];
    
            do {
                let i = value % base;
                output.unshift(digits[i]);
                value = Math.trunc(value / base);
            } while (value != 0);
    
            el.innerHTML = output.join("") + ' Creds';
        }else{
            el.innerHTML = 'Unknown Creds';
        }

    }
}