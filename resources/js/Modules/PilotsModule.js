
import axios from 'axios';
import settings from '../Api/settings';

export default {
    namespaced: true,
    state: { 
        pilots: [],
    },
    mutations: {
        SET_PILOTS(state, pilots){
            state.pilots = pilots;
        },
        DELETE_PILOT(state , pilotId){
            state.pilots.splice(state.pilots.findIndex(function(i){
                return i.id === pilotId;
            }), 1);
        }
    },
    actions:{
        getPilots({commit}){
            return axios.get(settings.url + '/pilots').then(response => {
                commit('SET_PILOTS' , response.data.results);
            }).catch(function (error) {
                this._vm.$vToastify.error('Error no controlado');
                console.log(error);
            });
        },
        deletePilot({commit} , pilotId){
            return axios.delete(settings.url + `/pilots/${pilotId}`).then(response => {
                this._vm.$vToastify.success(response.data.message);
                commit('DELETE_PILOT' , pilotId);
            }).catch(function (error) {
                this._vm.$vToastify.error('Error no controlado');
                console.log(error);
            });
        }
    },
}
