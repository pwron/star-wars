
import axios from 'axios';
import settings from '../Api/settings';

export default {
    namespaced: true,
    state: { 
        starships: [],
    },
    mutations: {
        SET_STARSHIPS(state, starships){
            state.starships = starships;
        },
    },
    actions:{
        getStarships({commit}){
            return axios.get(settings.url + '/starships').then(response => {
                commit('SET_STARSHIPS' , response.data.results);
            }).catch(function (error) {
                this._vm.$vToastify.error('Error no controlado');
                console.log(error);
            });
        },
        addPilot({dispatch} , payload){
            return axios.put(settings.url + `/starships/add-pilot/${payload.starshipId}` , {pilotId: payload.pilotId}).then(response => {
                this._vm.$vToastify.success(response.data.message);
                dispatch('getStarships');
            }).catch(function (error) {
                this._vm.$vToastify.error('Error no controlado');
                console.log(error);
            });
        },
        deletePilot({dispatch} , payload){
            return axios.put(settings.url + `/starships/delete-pilot/${payload.starshipId}` , {pilotId: payload.pilotId}).then(response => {
                this._vm.$vToastify.success(response.data.message);
                dispatch('getStarships');
            }).catch(function (error) {
                this._vm.$vToastify.error('Error no controlado');
                console.log(error);
            });
        }
    },
}
