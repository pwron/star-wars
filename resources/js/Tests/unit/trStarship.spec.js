import { mount } from '@vue/test-utils'
import trStartship from './../../Components/starships/tr-starship'
import base15 from "./../../Directives/base15";


test('Probando el calculador de precios en base15', () => {

  const wrapper = mount(trStartship, {
    propsData:{
      starship:{
        name: 'X-Wing',
        model: 'X-Wing',
        manufacturer: 'X-Wing',
        cost_in_credits: '3500000',
        pilots: [
          {
            id: 1,
            name: 'Lucke',
            gender: 'male',
          }
        ]
      }
    },
    directives: {
      base15
    }
  })

  //El precio inicial es de 3500000 en nuestro idioma y traducido para los demás tendría que ser 492085
  expect(wrapper.find('.precio').text()).toEqual('492085 Creds')
});