
import Starships from '../Pages/Starships';
import Pilots from '../Pages/Pilots';


const routes = [
    {
        path: '',
        component: Starships,
        name: 'starships'
    },
    {
        path: '/pilots',
        component: Pilots,
        name: 'pilots'
    },
]

export default routes;