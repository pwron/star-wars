<?php

namespace Tests\Feature;

use App\Models\Pilot;
use App\Models\Starship;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StarshipTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Comprobamos que devuelva una lista de naves y que el json devuelto concuerda con los datos de la BD.
     *
     * @return void
     */
    public function test_list_of_starships()
    {
        $this->withoutExceptionHandling();

        $starships = Starship::factory()->count(3)->create();

        $response = $this->get('/api/v1/starships');
        $response->assertStatus(200);

        foreach($starships as $starship){
            $response->assertJsonFragment([
                'id'     => $starship->id,
                'name'    => $starship->name,
                'model'    => $starship->model,
            ]);
        }

    }

    /**
     * Comprobamos la relación entre naves y pilotos.
     *
     * @return void
     */
    public function test_relation_pilot_in_starships()
    {
        $this->withoutExceptionHandling();

        $starships = Starship::factory()->count(3)->create();
        $pilot = Pilot::factory()->create();

        foreach($starships as $starship){
            $starship->pilots()->attach($pilot);

            $this->assertEquals($starship->pilots()->first()->id , $pilot->id);
        }
    }
}
