<?php

use App\Http\Controllers\Api\V1\PilotController;
use App\Http\Controllers\Api\V1\StarshipController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::get('/starships', [StarshipController::class, 'index']);
    Route::get('/starships/{starship}', [StarshipController::class, 'show']);
    Route::put('/starships/add-pilot/{starship}', [StarshipController::class, 'addPilot']);
    Route::put('/starships/delete-pilot/{starship}', [StarshipController::class, 'deletePilot']);

    Route::get('/pilots', [PilotController::class, 'index']);
    Route::get('/pilots/{pilot}', [PilotController::class, 'show']);
    Route::delete('/pilots/{pilot}', [PilotController::class, 'destroy']);
});