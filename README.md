## STAR WARS PROYECTO LARAVEL/VUE FULLSTACK

Iniciando proyecto

* He subido al repo también el archivo .env para que no tengas que generar la key 

```bash
    composer install
    npm install (opcional)
```

Iniciando la carga de la API

```bash
    php artisan crear-galaxia-muy-muy-lejana
```

Test 

```bash
    php artisan test
    npm run test
```

* Los test funcionales del backend hacen un Drop de las tablas, para probar creaciones y relaciones. 

