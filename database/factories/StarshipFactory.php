<?php

namespace Database\Factories;

use App\Models\Starship;
use Illuminate\Database\Eloquent\Factories\Factory;

class StarshipFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Starship::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'model' =>$this->faker->name(),
            'manufacturer' => $this->faker->name(),
            'cost_in_credits' => $this->faker->numberBetween(100 , 100000),
        ];
    }
}
