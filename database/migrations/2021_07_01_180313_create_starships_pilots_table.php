<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStarshipsPilotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('starships_pilots', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('pilot_id')->unsigned()->index();
            $table->foreign('pilot_id')
            ->references('id')->on('pilots')
            ->onDelete('cascade');

            $table->integer('starship_id')->unsigned()->index(); 
            $table->foreign('starship_id')
                ->references('id')->on('starships')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('starships_pilots');
    }
}
